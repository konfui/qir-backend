# Konfui - QiR Backend _(konfui-qir-backend)_

[![pipeline status](https://gitlab.com/konfui/qir-backend/badges/master/pipeline.svg)](https://gitlab.com/konfui/qir-backend/commits/master)
[![coverage report](https://gitlab.com/konfui/qir-backend/badges/master/coverage.svg)](https://gitlab.com/konfui/qir-backend/commits/master)

> Backend services hosted on Firebase for QiR mobile app.

This project provides backend that serves data for QiR mobile app. It uses
Google Sheets API and Cloud Functions on Firebase. Google Sheets serves as data
repository that will be used by events administrator to provide data that shall
be displayed in the mobile app. The mobile app then fetches the data from the
Google Sheets using API endpoint implemented as Cloud Functions on Firebase.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Backend Functions API](#backend-functions-api)
- [Clone to a New Project](#clone-to-a-new-project)
  - [Setup Google Cloud Platform & Firebase](#setup-google-cloud-platform--firebase)
  - [Create Google Sheets](#create-google-sheets)
- [Maintainers](#maintainers)
- [License](#license)

## Install

This project uses Node.js-based stack and Firebase. Additionally, it also uses
Python for deployment. You need to install:

- [Node.js v16+](https://nodejs.org/en/download/releases/)
  > Note: We recommend `nvm` (Node Version Manager) for installing
  > Node.js. You can find `nvm` for your OS at the following links:
  > - [nvm for macOS or GNU/Linux-based OS](https://github.com/creationix/nvm)
  > - [nvm for Windows](https://github.com/coreybutler/nvm-windows)
  >
  > Once `nvm` has been installed, install Node.js and activate it
  > by executing `nvm install 16.17.0` followed by `nvm activate 16.17.0`.
- [`yarn` package manager](https://yarnpkg.com/en/docs/install)
  > Note: You can install `yarn` using `npm` package manager provided by
  > standard Node.js installation. The installation command is
  > `npm install -g yarn`.
- [`firebase-tools` v11+](https://www.npmjs.com/package/firebase-tools)
  > Note: Install it globally by using `yarn`:
  > `yarn global add firebase-tools`.
- [Python v3+](https://www.python.org/downloads/)

Once you have installed all basic dependencies, clone the codebase and install
all required Node.js packages for the backend functions located in `functions`
folder.

- macOS or GNU/Linux-based OS

  ```bash
  # Assuming current working directory is at /home/MyName/
  git clone https://gitlab.com/konfui/qir-backend.git ~/qir-backend
  cd ~/qir-backend/functions
  yarn install
  ```

- Windows

  ```batch
  :: Assuming current working directory is at C:\Users\MyName\
  git clone https://gitlab.com/konfui/qir-backend.git qir-backend
  cd qir-backend/functions
  yarn install
  ```

Then, log `firebase` CLI tool (part of `firebase-tools` package) into Firebase
using your own credentials.

```bash
firebase login
```

> Note: Please ask the maintainer(s) to get Editors access into the app's
> Firebase project.

Once logged in, run the unit test suite in `functions` directory.

```bash
# Assuming current working directory is at ~/qir-backend (macOS /
# GNU/Linux-based OS) or C:\Users\MyName\qir-backend (Windows)
cd functions
yarn test
```

## Usage

You need to prepare a JSON file called `.runtimeconfig.json` that contains the
spreadsheet ID, data range, and Google Sheets API key for each data source that
will be served by the backend functions. Google Sheets API key can be obtained
via [Google Cloud Platform console](https://console.cloud.google.com).
Spreadsheet ID can be identified from the URL of the spreadsheet when viewed on
Web browser. For example, the spreadsheet ID of the Google Sheets that being
used for QiR conference is highlighted inside the red box in the following
screenshot:

![An example of spreadsheet ID of a Google Sheets](docs/images/spreadsheet_id_example.png)

> Note: If you do not have Google Cloud Platform account, you may request a
> temporary Google Sheets API key from the maintainers.

The format for `.runtimeconfig.json` is as follows:

```js
{
  "events": {
    "id": "SPREADSHEET_ID",
    "range": "Events!A1:J",
    "key": "GOOGLE_SHEETS_API_KEY"
  },
  "publications": {
    "id": "SPREADSHEET_ID",
    "range": "Submissions!A1:R",
    "key": "GOOGLE_SHEETS_API_KEY"
  },
  "sessions": {
    "id": "SPREADSHEET_ID",
    "range": "Tracks!A1:D",
    "key": "GOOGLE_SHEETS_API_KEY"
  },
  "speakers": {
    "id": "SPREADSHEET_ID",
    "range": "Speakers!A1:M",
    "key": "GOOGLE_SHEETS_API_KEY"
  },
  "authors": {
    "id": "SPREADSHEET_ID",
    "range": "Authors!A1:J",
    "key": "GOOGLE_SHEETS_API_KEY"
  }
}
```

> Note: For obvious reason, please remember to replace each occurence of
> `SPREADSHEET_ID` and `GOOGLE_SHEETS_API_KEY` with actual values. The `range`
> values only serve as examples in this document. The project assumes all data
> served from a single Google Sheets with multiple sheets. In Microsoft Excel
> speak, a single Google Sheets is equivalent to a workbook, while the sheet is
> the same in both products.

Once you created `.runtimeconfig.json`, place the file in `functions` directory.
Now you can deploy the backend functions as Firebase Functions by using the
custom Python scripts found in `ci` directory.

```bash
python ci/update_config.py functions/.runtimeconfig.json
python ci/deploy_functions.py
```

## Backend Functions API

See [README](functions/README.md) in `functions` directory.

## Clone to a New Project

If you want to use this codebase to provide backend services for another
conference, you need to:

1. Create a new project on Google Cloud Platform, which will be used for
   setting up Google Sheets API and also to associate with a new Firebase
   project
1. Prepare a Google Sheets with data structure similar to QiR conference's
   Google Sheets.

The detailed steps are explained in the following subsections.

### Setup Google Cloud Platform & Firebase

First, you need to setup a new project on Google Cloud Platform and enable
Google Sheets API. The steps are as follows:

1. Go to [Google Cloud Platform](https://cloud.google.com) and create a new
   project. Once created, switch to the new project immediately.
1. Go to [API Library](https://console.cloud.google.com/apis/library) and search
   for "Google Sheets API". Choose it from the search results and click
   **Enable API**.
1. Go to [API Credentials](https://console.cloud.google.com/apis/credentials)
   and choose to **Create API key**. Take note of the generated API key and
   keep it safe.
1. Secure the API key by limiting its access only to Google Sheets API.

The following animation depicts the steps from creating the API until securing
it.

![Animated guide on how to create API key and securing it](docs/images/gcp_api.gif)

Now you need to create a new Firebase project that associated with the new
project on Google Cloud Platform. The steps are as follows:

1. Go to [Firebase](https://console.firebase.google.com/) and add a new
   project. Choose a Google Cloud Platform that shall be associated with
   Firebase.
   > Note: Firebase will warn you that the new Firebase project will use
   > **Blaze** pricing plan. It allows free usage on many Firebase resources
   > as long the project does not exceed the predetermined quota. Please refer
   > to the Firebase documentation to know the how much free quota granted to
   > the project.
1. Skip Google Analytics creation step. You can add it later.
1. Close the Web browser and go to the codebase that you have create locally.
   Log the `firebase` CLI tools into the new Firebase project you just created.

   ```bash
   # Assuming the codebase for backend services are located at ~/qir-backend
   cd ~/qir-backend
   firebase login
   firebase use YOUR_FIREBASE_PROJECT_ALIAS_OR_PROJECT_ID
   ```

### Create Google Sheets

The steps for creating the Google Sheets is pretty much straightforward. You
need to make sure that the new Google Sheets document contains the same sheets
as the original Google Sheets (i.e. the backend for QiR conference). The
easiest way is by making a duplicate of QiR's Google Sheets and empty the
contents of all sheets in the duplicated Google Sheets. Then, you can fill
it with preliminary or sample data. Once done, take note of the spreadsheet ID
by looking for it in the URL bar of your Web browser. Then, edit the
`.runtimeconfig.json` in `functions` directory of the codebase and put the
spreadsheet ID and API keys in the configuration. Finally, you can deploy
the backend functions.

### Create Firebase Storage

This project uses Firebase Storage to serve static assets such as photos.
Please ensure that the storage only writable by authenticated users and
readable by everyone. Storage rules example as follows:

```
service firebase.storage {
  match /b/{bucket}/o {
    match /{allPaths=**} {
      allow read;
      allow write: if request.auth != null;
    }
  }
}
```

## Maintainers

- [Daya Adianto](https://gitlab.com/addianto)
- [Danny August](https://gitlab.com/daystram)
- [Meitya Dianti](https://gitlab.com/meitya)

## License

Copyright (C) 2019 [Pusilkom UI](https://pusilkom.ui.ac.id) for
[QiR conference](https://qir.eng.ui.ac.id) organised by the
[Faculty of Engineering Universitas Indonesia](http://eng.ui.ac.id).

This project is licensed under [GPLv3](LICENSE).
