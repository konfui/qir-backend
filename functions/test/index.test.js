const {describe} = require('mocha');
const {expect} = require('chai');
const sinon = require('sinon');
const HttpStatus = require('http-status-codes');
const test = require('firebase-functions-test')();
test.mockConfig({
    events: {
        id: "1bTO_love_ru",
        key: 'AI_za_warudo_wry',
        range: 'Events!A1:D22'
    },
    publications: {
        id: "1bTO_love_ru",
        key: 'AI_za_warudo_wry',
        range: 'Publications!A1:D22'
    },
    sessions: {
        id: "1bTO_love_ru",
        key: 'AI_za_warudo_wry',
        range: 'Publications!A1:D22'
    },
    speakers: {
        id: "1bTO_love_ru",
        key: 'AI_za_warudo_wry',
        range: 'Publications!A1:D22'
    }
});
const functions = require('../index');
const {FakeRequest, FakeResponse} = require('./fakes');

describe('Backend functions', function () {
    before(function () {
        sinon.stub(console, 'error');
    });
    after(function () {
        sinon.resetBehavior();
    });
    context('getEvents()', function () {
        it('handles invalid request', function () {
            verifyInvalidRequestHandling(functions.getEvents);
        });
        it('handles connection trouble', function () {
            return verifyConnectionTroubleHandling(functions.getEvents);
        });
    });
    context('publications()', function () {
        it('handles invalid request', function () {
            verifyInvalidRequestHandling(functions.publications);
        });
        it('handles connection trouble', function () {
            return verifyConnectionTroubleHandling(functions.publications);
        });
    });
    context('sessions()', function () {
        it('handles invalid request', function () {
            verifyInvalidRequestHandling(functions.sessions);
        });
        it('handles connection trouble', function () {
            return verifyConnectionTroubleHandling(functions.sessions);
        });
    });
    context('speakers()', function () {
        it('handles invalid request', function () {
            verifyInvalidRequestHandling(functions.speakers);
        });
        it('handles connection trouble', function () {
            return verifyConnectionTroubleHandling(functions.speakers);
        });
    });
    context('authors()', function () {
        it('handles invalid request', function () {
            verifyInvalidRequestHandling(functions.authors);
        });
        it.skip('handles connection trouble', function () {
            return verifyConnectionTroubleHandling(functions.authors);
        });
    });
    context('search()', function () {
        it('handles invalid request', function () {
            verifyInvalidRequestHandling(functions.search);
        });
        it.skip('handles connection trouble', function () {
            return verifyConnectionTroubleHandling(functions.search);
        });
    });
});

const verifyInvalidRequestHandling = (fun) => {
    const request = FakeRequest('application/vnd.api+xml');
    const response = FakeResponse();

    fun(request, response);

    expect(response._status).to.equal(HttpStatus.METHOD_NOT_ALLOWED);
};

const verifyConnectionTroubleHandling = (fun) => {
    const request = FakeRequest('application/vnd.api+json');
    const response = FakeResponse();

    return new Promise((resolve) => {
        const results = fun(request, response);
        resolve(results);
    }).catch(() => {
        expect(response._status).to.equal(HttpStatus.BAD_GATEWAY);
    });
};