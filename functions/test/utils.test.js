const {describe} = require('mocha');
const {expect} = require('chai');
const utils = require('../utils');

describe('Utility functions', function () {
    context('isValidRequest()', function () {
        const validContentType = 'application/vnd.api+json';
        it('returns true when given valid content type', function () {
            const fakeRequest = {
                headers: {
                    'content-type': validContentType,
                },
            };
            expect(utils.isValidRequest(fakeRequest, validContentType)).to.be.true;
        });
        it('returns false when given invalid content type', function () {
            const invalidContentType = 'text/html';
            const fakeRequest = {
                headers: {
                    'content-type': invalidContentType,
                },
            };
            expect(utils.isValidRequest(fakeRequest, validContentType)).to.be.false;
        });
        it('returns false when given request with empty content type', function () {
            const fakeRequest = {
                headers: {
                    'content-type': '',
                },
            };
            expect(utils.isValidRequest(fakeRequest, validContentType)).to.be.false;
        });
        it('returns false when given request not contain valid content type', function () {
            expect(utils.isValidRequest({}, validContentType)).to.be.false;
            expect(utils.isValidRequest({
                headers: {
                    'Content-type': '',
                },
            }, validContentType)).to.be.false;
        });
    });
    context('createObject()', function () {
        it('can create a data object given complete attributes', function () {
            const sampleAttributes = ['id', 'title', 'location'];
            const sampleRow = ['1', 'Lorem Ipsum', 'Somewhere'];

            expect(utils.createObject(sampleAttributes, sampleRow)).to.deep.equal({
                id: '1',
                title: 'Lorem Ipsum',
                location: 'Somewhere',
            });
        });
        it('can create a data object given sparse attributes', function () {
            const sampleAttributes = ['id', 'title', 'location', 'time'];
            const sampleRow1 = ['1', 'Lorem Ipsum'];
            const sampleRow2 = ['2', 'Dolor Sit', '', '12:00'];

            expect(utils.createObject(sampleAttributes, sampleRow1)).to.deep.equal({
                id: '1',
                title: 'Lorem Ipsum',
                location: '',
                time: '',
            });
            expect(utils.createObject(sampleAttributes, sampleRow2)).to.deep.equal({
                id: '2',
                title: 'Dolor Sit',
                location: '',
                time: '12:00',
            })
        });
    });
    context('sheetsToObjects()', function() {
        it('can transform 2D matrix (sheets) to list of objects', function() {
            const sampleSheets = [
                ['id', 'title', 'location', 'time'],
                ['1', 'Lorem Ipsum', 'Somewhere', '12:00'],
                ['2', '', 'Dolor Sit', '13:00'],
                ['3', 'ABC', '', '']
            ];
            expect(utils.sheetsToObjects((sampleSheets))).to.deep.equal([
                {
                    id: '1',
                    title: 'Lorem Ipsum',
                    location: 'Somewhere',
                    time: '12:00',
                },
                {
                    id: '2',
                    title: '',
                    location: 'Dolor Sit',
                    time: '13:00',
                },
                {
                    id: '3',
                    title: 'ABC',
                    location: '',
                    time: '',
                },
            ]);
        });
    });
    context('extractAuthors()', function() {
        it('can parse single author into list of authors', function() {
            const sampleData = [
                {
                    id: '1',
                    authors: 'Giorno Giovanna',
                },
                {
                    id: '2',
                    authors: 'Bruno Bucciarati',
                },
                {
                    id: '3',
                    authors: 'Leone Abbachio',
                }
            ];

            const authors = utils.extractAuthors(sampleData);

            expect(authors.length).to.equal(3);
        });
        it('can parse multiple authors into list of authors', function() {
            const sampleData = [
                {
                    id: '1',
                    authors: 'Giorno Giovanna',
                },
                {
                    id: '2',
                    authors: 'Bruno Bucciarati, Leone Abbachio',
                },
                {
                    id: '3',
                    authors: 'Guido Mista, Narancia Ghirga, Pannacota Fugo, Trish Una'
                }
            ];

            const authors = utils.extractAuthors(sampleData);

            expect(authors.length).to.equal(7);
        });
        it('can parse duplicate multiple authors into list of unique authors', function() {
            const sampleData = [
                {
                    id: '1',
                    authors: 'Giorno Giovanna, Bruno Bucciarati',
                },
                {
                    id: '2',
                    authors: 'Bruno Bucciarati, Leone Abbachio, Guido Mista',
                },
                {
                    id: '3',
                    authors: 'Guido Mista, Pannacota Fugo, Trish Una, Giorno Giovanna, Bruno Bucciarati'
                }
            ];

            const authors = utils.extractAuthors(sampleData);

            expect(authors.length).to.equal(6);
        });
        it('returns the list of authors sorted alphabetically', function() {
            const sampleData = [
                {
                    id: '1',
                    authors: 'Dio Brando, Giorno Giovanna',
                },
                {
                    id: '2',
                    authors: 'Bruno Bucciarati, Guido Mista',
                },
                {
                    id: '3',
                    authors: 'Joseph Joestar, Jonathan Joestar',
                }
            ];

            const authors = utils.extractAuthors(sampleData);


            expect(authors.length).to.equal(6);
            expect(authors).to.deep.equal([
                'Bruno Bucciarati', 'Dio Brando', 'Giorno Giovanna', 'Guido Mista',
                'Jonathan Joestar', 'Joseph Joestar'
            ]);
        });
        it('can handle authors with initials', function() {
            const sampleData = [
                {
                    id: '1',
                    authors: 'Robert E. O. Speedwagon, Will A. Zeppeli',
                },
                {
                    id: '2',
                    authors: 'D. Oh, J. Jo',
                }
            ];

            const authors = utils.extractAuthors(sampleData);

            expect(authors.length).to.equal(4);
            expect(authors.includes('Robert E. O. Speedwagon')).to.be.true;
            expect(authors.includes('Will A. Zeppeli')).to.be.true;
            expect(authors.includes('D. Oh')).to.be.true;
            expect(authors.includes('J. Jo')).to.be.true;
        });
        it('can handle authors with single word', function() {
            const sampleData = [
                {
                    id: '1',
                    authors: 'Esidisi',
                },
                {
                    id: '2',
                    authors: 'Wammu, Aerosmith',
                },
                {
                    id: '3',
                    authors: 'Avdol, Dio Brando',
                }
            ];

            const authors = utils.extractAuthors(sampleData);

            expect(authors.length).to.equal(5);
            expect(authors.includes('Dio Brando')).to.be.true;
            expect(authors.includes('Esidisi')).to.be.true;
            expect(authors.includes('Brando')).to.be.false;
        });
        it("can handle multiple authors with 'and' keyword", function() {
            const sampleData = [
                {
                    id: '1',
                    authors: 'Robert E. O. Speedwagon and Will A. Zeppeli',
                },
                {
                    id: '2',
                    authors: 'D. Oh and Janda Jo',
                },
                {
                    id: '3',
                    authors: 'Guido Mista, Pannacota Fugo, Trish Una, Giorno Giovanna, and Bruno Bucciarati',
                }
            ];

            const authors = utils.extractAuthors(sampleData);

            expect(authors.length).to.equal(9);
            expect(authors.includes('Janda Jo')).to.be.true;
            expect(authors.includes('Bruno Bucciarati')).to.be.true;
            expect(authors.some(value => value.search(/and /) > 0)).to.be.false;
        });
    });
    context('filterUnique()', function() {
        it('filters duplicates to unique items', function() {
            const sampleData = [
                {
                    fullName: 'Giorno Giovanna',
                    email: 'giorno@passione.it',
                },
                {
                    fullName: 'Giorno Giovanna',
                    email: 'giorno@passione.it',
                },
                {
                    fullName: 'Giorno Giovanna',
                    email: 'giorno@passione.it',
                },
                {
                    fullName: 'Guido Mista',
                    email: 'mista@passione.it',
                },
                {
                    fullName: 'Pannacota Fugo',
                    email: 'fugo@passione.it',
                },
            ];

            const results = utils.filterUnique(sampleData, ['fullName', 'email']);

            expect(results.length).to.equal(3);
            expect(results.filter(e => e.fullName === 'Giorno Giovanna').length).to.equal(1);
            expect(results.filter(e => e.fullName === 'Guido Mista').length).to.equal(1);
            expect(results.filter(e => e.fullName === 'Pannacota Fugo').length).to.equal(1);
        });
    });
});
