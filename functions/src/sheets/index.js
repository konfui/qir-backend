// SPDX-License-Identifier: GPL-3.0-or-later
'use strict'
const {google} = require('googleapis');
const API_VERSION = 'v4';
const googleSheetsAPI = google.sheets(API_VERSION);

const getValues = (key, id, range) => {
    const options = {
        auth: key,
        spreadsheetId: id,
        range: range,
        majorDimension: 'ROWS'
    };

    return new Promise((resolve, reject) => {
        googleSheetsAPI.spreadsheets.values.get(options, (error, valueRange) => {
            if (error) {
                const message = 'There was a trouble communicating with Google Sheets';
                console.error('sheets#getValues()', message)
                reject(error);
            }
            
            const {values} = valueRange.data;
            console.log('sheets#getValues()', '# of entries', values.length);
            resolve(values);
        });
    });
};

exports.getValues = getValues;
