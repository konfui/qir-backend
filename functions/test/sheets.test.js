const {describe} = require('mocha');
const {expect} = require('chai');
const sheets = require('../src/sheets');

describe('Sheets module', function() {
    context('getValues()', function() {
        it('returns a Promise object', function() {
            const values = sheets.getValues('A', 'B', 'C');

            expect(values).to.be.a('Promise');
        });
    });
});
